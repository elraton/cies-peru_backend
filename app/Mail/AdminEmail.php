<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'no-reply@cies-peru.com';
        $subject = 'Un nuevo participante se registró';
        $name = 'no reply CIES PERU';
        
        return $this->view('emails.adminemail')
                    ->from($address, $name)
                    ->subject($subject)
                    ->with([ 
                        'adminmessage' => $this->data['message2'],
                        'name' => $this->data['name'],
                        'email' => $this->data['email'],
                        'phone' => $this->data['phone'],
                        'subject' => $this->data['subject']
                    ]);
    }
}
