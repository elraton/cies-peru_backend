<!doctype html>
<html>
  <head>
    <title>CIES - PERU | Escuela de entrenamiento de posturología clínica </title>
    <meta charset="UTF-8">
    <meta name="Robots" content="all"/>
    <meta name="google" content="nositelinkssearchbox">
    <meta name="author" content="elratonmaton@gmail.com">
    <meta name="description" content="Escuela de entrenamiento de posturología clínica, El C.I.E.S es una asociación de posturología clínica creada en Marsella en 1985 por el Doctor Bernard Bricot" />
    <meta http-equiv="Content-Language" content="es"/>
    <meta name="distribution" content="global"/>
    <meta name="revisit-after" content="7 days">
    <meta name="viewport" content="width=device-width, user-scalable=no">
  </head>
  <body>
    <script src="bundle.js"></script>

    <div class="header">
      <div class="spacer"></div>
      <div class="content">
        <a class="logo_header" href="#"><img src="imgs/logo-cies.png"></a>
        <div class="menu_cont">
          <ul class="menu">
            <li class="item">
              <a href="#page_about" class="link">Sobre el Evento</a>
            </li>
            <li class="item">
              <a href="#page_ponents" class="link">Ponentes</a>
            </li>
            <li class="item">
              <a href="#page_program" class="link" disabled>Programa</a>
            </li>
            <li class="item">
              <a href="#page_contact" class="link">Informes</a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div id="page_home">
      
      <div class="banner">
        <div class="floating-top">
          <img src="imgs/banner_top.png">
        </div>
        <div class="floating-left">
          <img src="imgs/banner_left.png">
        </div>
        <div class="floating-bottom">
          <img src="imgs/banner_bottom.png">
        </div>
        
        <div class="title">
          <h5>POSTUROLOGÍA CLÍNICA</h5>
          <h6>RECALIBRACIÓN POSTURAL</h6>
        </div>
        
        <div class="subtitles">
          <h4>Principios de la Posturología</h4>
          <h4>Avances en Posturología</h4>
          <h4>Reflejos Arcaicos</h4>
        </div>
      </div>
    </div>

    <div id="page_about">
      <h4 class="title">Sobre el evento</h4>
      <p>Este es un curso acreditado internacionalmente por el Collège International d`étude de la Statique Francia.</p>
      <br>
      <p>Objetivos del curso:</p>
      <p class="list">Brindar a los profesionales de la salud los conocimientos necesarios para abordar las diversas patologías de una manera global desde el punto de vista de la Posturología Clínica.</p>
      <p class="list">Desarrollar un criterio de atención interdisciplinario, en el cual todos los profesionales conozcan la importancia de la anamnesis desde el punto de vista de la posturología clínica.</p>
      <p class="list">Analizar y entender el sistema tónico postural y su neurofisiología.</p>
      <p class="list">Mostrar las consecuencias funcionales y anatómicas de las disfunciones posturales.</p>
      <p class="list">Estudiar los bloqueos, los obstáculos a la Reprogramación Postural Global.</p>
      <p class="list">Entender la importancia de recalibrar la información que ingresa al cerebro por los captores posturales y como su tratamiento adecuado generará cambios en el organismo y a nivel estructural.</p>
      <p class="list">Aprender a hacer una valoración postural y establecer diferentes protocolos terapéuticos que tendrán en cuenta ese desequilibrio tónico postural y los diferentes captores desajustados.</p>
    </div>

    <div id="page_ponents">
      <h4 class="title">Expositores</h4>
      
      <div class="cards">
        <div class="prev">
          <a id="ponents_prev">
            <img class="arrow" src="imgs/flecha_black.png">
          </a>
        </div>
        <div class="next">
          <a id="ponents_next">
            <img class="arrow" src="imgs/flecha_black.png">
          </a>
        </div>

        <div class="card_carousel">
            <div class="card_cont active">
              <div class="ponent_card bricot">
                <div class="photo_block">
                  <img src="imgs/britcot.png">
                </div>
                <div class="text">
                  <div class="name">
                    <h1>Dr. Bernard Bricot</h1>
                  </div>
                  <div class="description">
                    <p>Referente mundial y creador del método Reprogramación postural y de la escuela más importante del mundo en posturología.</p>
                    <p>Doctor en cirugía ortopédica y especialista en posturología y estática. Es el Director del CIES-Marsella (Collège International d'Étude de la Statique). Colabora como profesor en el curso de posgrado de posturología y podoposturología de la Universitat de Barcelona.</p>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="card_cont">
              <div class="ponent_card thomas">
                <div class="photo_block">
                  <img src="imgs/thomas.png">
                </div>
                <div class="text">
                  <div class="name">
                    <h1>Dr. Thomas Solente</h1>
                  </div>
                  <div class="description">
                    <p>Médico Cirujano diplomado de la Facultad de Medicina de la
                        Universidad Nacional de Asunción – Paraguay, especializado
                        en Francia. Es Presidente del CIES Paraguay -
                        Collège International d`Étude de la Statique – y docente del
                        Método BRICOT de Recalibración Postural Global.</p>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="card_cont">
              <div class="ponent_card cecile">
                <div class="photo_block">
                  <img src="imgs/cecile.png">
                </div>
                <div class="text">
                  <div class="name">
                    <h1>Dra. Cécile Mathieu</h1>
                  </div>
                  <div class="description">
                    <p>Osteopata Tradicional y Pediatrica Diplomada en
                        Posturología por el CiES Francia y especializada en la
                        Integración de los Reflejos Arcaicos.</p>
                    <p>Es Autora de “La contribución del reflejo arcaico en la
                        Posturología Clínica” y Directora del centro de Posturología
                        de Ginebra Suiza.</p>
                  </div>
                </div>
              </div>
            </div>
        </div>
  
      </div>

    </div>

    <div id="page_program">
      <h4 class="title">Programa</h4>

      <div class="content">
        <div class="cursotitle">
          <h4>Modulo I</h4>
        </div>
  
        <div class="schedule">
          <div class="header">
            <a class="prev">
              <img src="imgs/flecha_white.png">
            </a>
            <span class="schedule_title">Fundamentos de la Posturología</span>
            <a class="next">
              <img src="imgs/flecha_white.png">
            </a>
          </div>
  
          <div class="subgroup">
            <div class="turn">
              <div class="morning">
                <h4></h4>
              </div>
              <div class="afternoon">
                <h4></h4>
              </div>
            </div>

            <div class="schedule_text">
              <div class="list_left">
                <div class="item">
                  <p>Estática normal y patológica en los 3 planos del espacio (frontal, sagital y rotatorio).</p>
                </div>
                <div class="item">
                  <p>Las consecuencias de los trastornos de la estática.</p>
                </div>
                <div class="item">
                  <p>El doble péndulo fractal</p>
                </div>
                <div class="item">
                  <p>La neurofisiología del sistema tónico postural</p>
                </div>
                <div class="item">
                  <p>Las indicaciones para la reprogramación postural</p>
                </div>
                <div class="item">
                  <p>Los principales sensores del cuerpo humano: nociones fundamentales</p>
                </div>
              </div>
              <div class="vert_serparator">
                <span></span>
              </div>
              <div class="list_right">
                <div class="item">
                  <p>Los obstáculos a la recalibración postural</p>
                </div>
                <div class="item">
                  <p>Los bloqueos fuera de sistema: diferentes tipos de tratamientos</p>
                </div>
                <div class="item">
                  <p>El examen postural completo: anamnesis, tipo de postura, evaluación de todos los sensores</p>
                </div>
                <div class="item">
                  <p>Las consecuencias del trastorno postural: patologías del aparato locomotor y patologías generales</p>
                </div>
                <div class="item">
                  <p>Bases para la recalibración postural</p>
                </div>
              </div>
            </div>
      
          </div>
  
        </div>
      </div>
    </div>

    <div id="page_contact">
      <h4 class="title">Informes</h4>

      <div class="content">
        <div class="contact-info">
          <div class="info">
            <div class="phone">
              <img src="imgs/call-answer.png">
              <span>Teléfono</span>
            </div>
            <div class="email">
              <img src="imgs/envelope.png">
              <span>Email</span>
            </div>
          </div>

          <div class="info">
            <div class="phone">
              <span>941 571 577</span>
            </div>
            <div class="email">
              <span>informes@cies-peru.com</span>
            </div>
          </div>
          <div class="info fb-follow">
            <div class="phone">
              <span class="follow">Nuestras redes:</span>
            </div>
            <div class="email">
              <a href="https://www.facebook.com/CiesPosturologiaPeru/" target="_blank"><img class="fb" src="imgs/facebook_circle-512.png"></a>
              <a href="https://wa.me/51941571577" target="_blank"><img class="fb" src="imgs/whatsapp.png"></a>
            </div>
          </div>

          <div class="logo-con">
            <img src="imgs/logo-cies.png" class="logo">
          </div>

        </div>
        <div class="form" id="form_info">
          @csrf
          <div class="content">
            <h4 class="title">Informes</h4>
            <div class="form-row">
              <input type="text" id="form_name" name="name" placeholder="Nombre">
            </div>
            <div class="form-row2">
              <input type="email" id="form_email" name="email" placeholder="Email">
              <input type="phone" id="form_phone" name="phone" placeholder="Teléfono">
            </div>
            <div class="form-row">
              <input type="text" id="form_subject" name="subject" placeholder="Asunto">
            </div>
            <div class="form-row">
              <textarea placeholder="Mensaje" id="form_message" name="message" col="10"></textarea>
            </div>
            <div class="form-row">
              <button type="submit" class="submit-button">Enviar</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="snackbar"></div>


  </body>
</html>